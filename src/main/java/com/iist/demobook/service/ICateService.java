package com.iist.demobook.service;

import com.iist.demobook.model.ResultResp;

/**
 * @author by LeoJr
 * 3:15 PM 9/5/2019
 **/

public interface ICateService {

    ResultResp getAllCate();
}
