package com.iist.demobook.service;

import com.iist.demobook.model.ResultResp;
import com.iist.demobook.model.req.BookReq;
import com.iist.demobook.model.req.BookUpdateReq;
import com.iist.demobook.model.req.DeleteBookReq;
import com.iist.demobook.model.req.SearchParamReq;

/**
 * @author by LeoJr
 * 2:47 PM 9/3/2019
 **/

public interface IBookService {

    ResultResp creatNewBook(BookReq book);

    ResultResp getBookCate(Integer cate);

    ResultResp updateBookById(BookUpdateReq bookUpdateReq);

    ResultResp deleteBookById(DeleteBookReq deleteBookReq);

    ResultResp search(SearchParamReq searchParamReq);
}
