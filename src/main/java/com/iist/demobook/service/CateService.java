package com.iist.demobook.service;

import com.iist.demobook.model.ResultResp;
import com.iist.demobook.model.core.RespType;
import com.iist.demobook.model.entity.TblCate;
import com.iist.demobook.repo.ICateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author by LeoJr
 * 3:22 PM 9/5/2019
 **/

@Service
public class CateService implements ICateService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected ICateRepository cateRepository;

    @Override
    public ResultResp getAllCate() {

        ResultResp result = ResultResp.notFound();

        try {
            Collection<TblCate> cates = new ArrayList<TblCate>();
            Iterable<TblCate> tblCateOptional = cateRepository.findAll();
            tblCateOptional.forEach(cates::add);
            result = ResultResp.success(cates);
        } catch (Exception e) {
            logger.info("<=== " + this.getClass().toString() + " getAllCate Err: ");
            logger.info(e.toString());

            result = ResultResp.fail(RespType.DATABASE_COMMIT_ERR);
        }

        return result;
    }
}
