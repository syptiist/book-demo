package com.iist.demobook.service;

import com.iist.demobook.model.ResultResp;
import com.iist.demobook.model.core.RespType;
import com.iist.demobook.model.entity.TblBook;
import com.iist.demobook.model.req.BookReq;
import com.iist.demobook.model.req.BookUpdateReq;
import com.iist.demobook.model.req.DeleteBookReq;
import com.iist.demobook.model.req.SearchParamReq;
import com.iist.demobook.model.resp.BookResp;
import com.iist.demobook.repo.IBookRepository;
import com.iist.demobook.repo.custom.IBookCustomRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author by LeoJr
 * 2:48 PM 9/3/2019
 **/

@Service
public class BookService implements IBookService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IBookRepository bookRepository;

    @Autowired
    private IBookCustomRepo bookCustomRepo;

    @Override
    public ResultResp creatNewBook(BookReq book) {
        ResultResp result = ResultResp.notFound();

        String bookId = UUID.randomUUID().toString();
        TblBook tblBook = new TblBook(bookId, book.getBookName().trim(), book.getCateId(), book.getPrice());
        tblBook.setCreatedTime(new Date());
        tblBook.setUpdatedTime(new Date());

        try {
            bookRepository.save(tblBook);

            result = ResultResp.success(tblBook);
        } catch (Exception e) {
            logger.info("<========== creatNewBook Err: ");
            logger.info(e.toString());
            result = ResultResp.fail(RespType.DATABASE_COMMIT_ERR);
        }
        return result;
    }

    @Override
    public ResultResp getBookCate(Integer cateId) {
        ResultResp result = ResultResp.notFound();

        try {
            Iterable<BookResp> books;
            if (cateId != null && cateId > 0) {
                books = bookCustomRepo.listAllByCateId(cateId);

            } else
                books = bookCustomRepo.findAll();

            result = ResultResp.success(books);
        } catch (Exception e) {
            logger.info("<========== creatNewBook Err: ");
            logger.info(e.toString());
            result = ResultResp.fail(RespType.DATABASE_COMMIT_ERR);
        }
        return result;
    }

    @Override
    public ResultResp updateBookById(BookUpdateReq book) {
        ResultResp result = ResultResp.notFound();

        try {
            Optional<TblBook> tblBookOptional = bookRepository.findById(book.getBookId());
            if (tblBookOptional.isPresent()) {
                TblBook tblBook = tblBookOptional.get();
                tblBook.setUpdatedTime(new Date());
                tblBook.setCateId(book.getCateId());
                tblBook.setBookName(book.getBookName());
                tblBook.setPrice(book.getPrice());

                bookRepository.save(tblBook);

                result = ResultResp.success(tblBook);
            }
        } catch (Exception e) {
            logger.info("<========== creatNewBook Err: ");
            logger.info(e.toString());
            result = ResultResp.fail(RespType.DATABASE_COMMIT_ERR);
        }
        return result;
    }

    @Override
    public ResultResp deleteBookById(DeleteBookReq deleteBookReq) {
        ResultResp result = ResultResp.notFound();

        try{
            Optional<TblBook> tblBookOptional = bookRepository.findById(deleteBookReq.getBookId());
            tblBookOptional.ifPresent(tblBook -> bookRepository.delete(tblBook));
            result = ResultResp.success(null);
        }catch (Exception e){
            logger.info(e.toString());
            result = ResultResp.fail(RespType.DATABASE_COMMIT_ERR);
        }

        return result;
    }

    @Override
    public ResultResp search(SearchParamReq searchParamReq) {
        ResultResp result = ResultResp.notFound();

        try{
            List<BookResp> bookResps = bookCustomRepo.findCustomParam(searchParamReq);
            if(bookResps!=null && !bookResps.isEmpty()){
                result = ResultResp.success(bookResps);
            }

        }catch (Exception e){
            logger.info(e.toString());
            result = ResultResp.fail(RespType.DATABASE_COMMIT_ERR);
        }

        return result;
    }
}
