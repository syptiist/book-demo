package com.iist.demobook.exception;

import com.iist.demobook.model.core.RespType;
import com.iist.demobook.model.ResultResp;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author by LeoJr
 * 3:26 PM 9/3/2019
 **/

@Log4j2
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    public ResultResp missingServletRequestParameterException(MissingServletRequestParameterException ex) {
        log.error("missing servlet request parameter exception:{}", ex.getMessage());
        return ResultResp.fail(RespType.ARG_INVALID);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResultResp serviceException(MethodArgumentNotValidException ex) {
        log.error("service exception:{}", ex.getMessage());
        return ResultResp.fail(RespType.ARG_INVALID);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    public ResultResp baseException(RuntimeException ex) {
        log.error("base exception:{}", ex.getMessage());
        return ResultResp.fail(RespType.INTERNAL_SYSTEM);
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResultResp exception(Exception ex) {
        log.error("base exception:{}", ex.getMessage());
        return ResultResp.fail(RespType.INTERNAL_SYSTEM);
    }

    @ExceptionHandler(value = {Throwable.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResultResp throwable() {

        return ResultResp.fail(RespType.INTERNAL_SYSTEM);
    }
}
