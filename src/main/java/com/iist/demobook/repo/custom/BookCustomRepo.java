package com.iist.demobook.repo.custom;

import com.iist.demobook.model.req.SearchParamReq;
import com.iist.demobook.model.resp.BookResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author by LeoJr
 * 11:45 AM 9/4/2019
 **/

@Repository
public class BookCustomRepo extends BaseCustomRepo implements IBookCustomRepo {

    @Autowired
    private EntityManager entityManager;

    public BookCustomRepo() {

    }

    public List<BookResp> listAllByCateId(Integer cateId) {
        String sql = "select a.book_id, a.book_name, b.cate_name, a.price, a.cate_id from tbl_book a"
                + " inner join tbl_cate b on a.cate_id = b.id where a.cate_id = :cateId order by a.book_name asc";
        Query query = entityManager.createNativeQuery(sql, BookResp.class);
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("cateId", cateId);
        setParams(query, params);
        return query.getResultList();
    }

    @Override
    public List<BookResp> findAll() {
        String sql = "select a.book_id, a.book_name, b.cate_name, a.price, a.cate_id from tbl_book a"
                + " inner join tbl_cate b on a.cate_id = b.id order by a.book_name,b.id ASC ";
        Query query = entityManager.createNativeQuery(sql, BookResp.class);
        return query.getResultList();
    }

    public List<BookResp> findCustomParam(SearchParamReq searchParamReq) {

        searchParamReq.trims();
        Map<String, Object> params = new ConcurrentHashMap<>();

        String sql = "select a.book_id, a.book_name, b.cate_name, a.price from tbl_book a" +
                " inner join tbl_cate b on a.cate_id = b.id ";
        String order = " order by a.book_name,b.id ASC ";

        if (searchParamReq.getBookName() != null && !searchParamReq.getBookName().isEmpty()) {
            sql += " where a.book_name like :bookName ";
            params.put("bookName", "%" + searchParamReq.getBookName() + "%");
        }

        sql += order;

        Query query = entityManager.createNativeQuery(sql, BookResp.class);
        setParams(query, params);
        return query.getResultList();
    }
}
