package com.iist.demobook.repo.custom;

import com.iist.demobook.model.req.SearchParamReq;
import com.iist.demobook.model.resp.BookResp;

import java.util.List;

/**
 * @author by LeoJr
 * 1:25 PM 9/4/2019
 **/

public interface IBookCustomRepo {
    List<BookResp> listAllByCateId(Integer cateId);

    List<BookResp> findAll();

    List<BookResp> findCustomParam(SearchParamReq searchParamReq);
}
