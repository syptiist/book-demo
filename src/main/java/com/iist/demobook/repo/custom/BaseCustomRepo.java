package com.iist.demobook.repo.custom;

import javax.persistence.Query;
import java.util.Iterator;
import java.util.Map;

/**
 * @author by LeoJr
 * 1:16 PM 9/4/2019
 **/

public class BaseCustomRepo {
    protected void setParams(Query query, Map<String, Object> params) {
        Iterator var3 = params.entrySet().iterator();

        while (var3.hasNext()) {
            Map.Entry<String, Object> entry = (Map.Entry) var3.next();
            query.setParameter( entry.getKey(), entry.getValue());
        }
    }
    
     protected  String getLikeCondition(String str) {
        if (str == null) {
            str = "";
        }
        if (!str.trim().isEmpty()) {
            String newStr =
                str.trim()
                    .replace("\\", "\\\\")
                    .replace("\\t", "\\\\t")
                    .replace("\\n", "\\\\n")
                    .replace("\\r", "\\\\r")
                    .replace("\\z", "\\\\z")
                    .replace("\\b", "\\\\b")
                    .replaceAll("_", "\\\\_")
                    .replaceAll("%", "\\\\%");
            str = "%".concat(newStr.trim()).concat("%");
        }
        return str;
    }
}
