package com.iist.demobook.repo;

import com.iist.demobook.model.entity.TblCate;
import org.springframework.data.repository.CrudRepository;

/**
 * @author by LeoJr
 * 3:05 PM 9/5/2019
 **/

public interface ICateRepository extends CrudRepository<TblCate, Integer> {
}
