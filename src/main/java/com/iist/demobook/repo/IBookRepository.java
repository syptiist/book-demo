package com.iist.demobook.repo;

import com.iist.demobook.model.entity.TblBook;
import org.springframework.data.repository.CrudRepository;

/**
 * @author by LeoJr
 * 2:54 PM 9/3/2019
 **/

public interface IBookRepository extends CrudRepository<TblBook,String> {

}
