package com.iist.demobook.model.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author by LeoJr
 * 11:08 AM 9/4/2019
 **/

@Data
@Entity
public class BookResp {

    @Id
    @JsonProperty("book_id")
    private String bookId;

    @JsonProperty("book_name")
    private String bookName;

    @JsonProperty("cate_name")
    private String cateName;

    @JsonProperty("price")
    private Integer price;

    @JsonProperty("cate_id")
    private Integer cateId;
}
