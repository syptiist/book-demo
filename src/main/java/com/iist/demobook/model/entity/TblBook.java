package com.iist.demobook.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author by LeoJr
 * 2:39 PM 9/3/2019
 **/

@Entity
@Data
@Table(name = "tbl_book")
public class TblBook {

    @Id
    @Column(name = "book_id")
    @JsonProperty("book_id")
    private String bookId;

    @Column(name = "book_name")
    @JsonProperty("book_name")
    private String bookName;

    @Column(name = "cate_id")
    @JsonProperty("cate_id")
    private Integer cateId;

    @Column(name = "price")
    @JsonProperty("price")
    private Integer price;

    @Column(name = "created_time")
    @JsonProperty("created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "updated_time")
    @JsonProperty("updated_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;

    public TblBook(){

    }
    public TblBook(String bookId, String bookName, int cateId, int price){
        this.bookId = bookId;
        this.bookName = bookName;
        this.cateId = cateId;
        this.price = price;
    }
}
