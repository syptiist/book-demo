package com.iist.demobook.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * @author by LeoJr
 * 2:49 PM 9/5/2019
 **/

@Entity
@Data
public class TblCate {

    @Id
    @GeneratedValue
    @Column(name = "id")
    @JsonProperty("cate_id")
    private Integer cateId;

    @Column(name = "cate_name")
    @JsonProperty("cate_name")
    private String cateName;

    @Column(name = "created_time")
    @JsonProperty("created_time")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "updated_time")
    @JsonProperty("updated_time")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;

    public TblCate(){}

}
