package com.iist.demobook.model.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author by LeoJr
 * 4:45 PM 9/4/2019
 **/

@Data
public class SearchParamReq {

    @JsonProperty("book_name")
    private String bookName;

    public void trims(){
        if(bookName!=null){
            bookName.trim();
        }
    }
}
