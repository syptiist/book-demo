package com.iist.demobook.model.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author by LeoJr
 * 4:41 PM 9/3/2019
 **/

@Data
public class BookUpdateReq {
    @NotNull
    @NotEmpty
    @NotBlank
    @JsonProperty("book_id")
    private String bookId;

    @NotBlank
    @JsonProperty("book_name")
    private String bookName;

    @NotNull
    @JsonProperty("cate_id")
    private Integer cateId;

    @NotNull
    @JsonProperty("price")
    private Integer price;
}
