package com.iist.demobook.model.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author by LeoJr
 * 3:17 PM 9/4/2019
 **/

@Data
public class DeleteBookReq {

    @JsonProperty("book_id")
    private String bookId;

}
