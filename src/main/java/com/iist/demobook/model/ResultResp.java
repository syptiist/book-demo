package com.iist.demobook.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.iist.demobook.model.core.RespType;
import com.iist.demobook.model.core.BaseObject;

/**
 * @author by LeoJr
 * 1:34 PM 9/3/2019
 **/

public class ResultResp<T> extends BaseObject {
    @JsonProperty("code")
    private int code;

    @JsonProperty("msg")
    private String msg;

    @JsonProperty("data")
    private T data;

    public ResultResp() {

    }

    private ResultResp(RespType respType, T data) {
        this.code = respType.getCode();
        this.msg = respType.getMsg();
        this.data = data;
    }

    private ResultResp(RespType respType) {
        this.code = respType.getCode();
        this.msg = respType.getMsg();
    }

    public static ResultResp<Object> success(Object data) {
        return new ResultResp<>(RespType.SUCCESS, data);
    }

    public static ResultResp notFound(){
        return new ResultResp(RespType.FAIL,null);
    }

    public static ResultResp fail(RespType respType){
        return new ResultResp(respType);
    }
}
