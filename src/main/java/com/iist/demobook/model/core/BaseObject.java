package com.iist.demobook.model.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

/**
 * @author by LeoJr
 * 3:02 PM 8/27/2019
 **/
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseObject {
    public String toJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper().setAnnotationIntrospector(new JacksonAnnotationIntrospector());
        return mapper.writeValueAsString(this);
    }

    public static <T> T fromJson(String json, Class<T> clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper().setAnnotationIntrospector(new JacksonAnnotationIntrospector());
        return mapper.readValue(json, clazz);
    }
}
