package com.iist.demobook.model.core;

import lombok.Getter;

/**
 * @author by LeoJr
 * 1:44 PM 9/3/2019
 **/

@Getter
public enum RespType {
    SUCCESS(200,"OK"),
    FAIL(404,"Not Found"),
    ARG_INVALID(401,"Argument Invalid"),
    INTERNAL_SYSTEM(500, "Internal System"),
    DATABASE_COMMIT_ERR(402, "Database Error");

    private int code;
    private String msg;

    RespType(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
