package com.iist.demobook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoBookApplication.class, args);
        Logger logger = LoggerFactory.getLogger(DemoBookApplication.class);
        logger.info("---------------------STARTING APPLICATION--------------------------------");
        long maxBytes = Runtime.getRuntime().maxMemory();
        logger.info("----------->Max memory: " + maxBytes / 1024 / 1024 + "M");
    }

}
