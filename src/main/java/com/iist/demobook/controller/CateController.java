package com.iist.demobook.controller;

import com.iist.demobook.model.ResultResp;
import com.iist.demobook.service.ICateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by LeoJr
 * 3:12 PM 9/5/2019
 **/

@RestController
@RequestMapping("/cate")
public class CateController extends BaseController {

    @Autowired
    protected ICateService cateService;

    @GetMapping("/get-all")
    public ResultResp getAllCates(){
        logger.info("<===== " + this.getClass().toString() + " getAllCates: ");
        return cateService.getAllCate();
    }

}
