package com.iist.demobook.controller;

import com.iist.demobook.model.req.BookUpdateReq;
import com.iist.demobook.model.req.DeleteBookReq;
import com.iist.demobook.model.req.SearchParamReq;
import com.iist.demobook.service.IBookService;
import com.iist.demobook.model.ResultResp;
import com.iist.demobook.model.req.BookReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author by LeoJr
 * 2:56 PM 9/3/2019
 **/


@RestController
@RequestMapping("/book")
public class BookController extends BaseController {

    @Autowired
    protected IBookService bookService;

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResultResp createNewBook(@Valid @RequestBody BookReq book) {
        logger.info("<===== " + this.getClass().toString() + ": new book req: " + book.toString());
        return bookService.creatNewBook(book);
    }

    @GetMapping(value = "/get-book-by-cate")
    public ResultResp getBookByCate(@RequestParam("cate_id") Integer cateId){

        logger.info("<===== " + this.getClass().toString() + ": get book by cateId=" + cateId);
        return bookService.getBookCate(cateId);
    }

    @PostMapping(value = "/update-by-id")
    public ResultResp updateById(@Valid @RequestBody BookUpdateReq bookUpdateReq){
        logger.info("<===== " + this.getClass().toString() + ": update book req: " + bookUpdateReq.toString());
        return bookService.updateBookById(bookUpdateReq);
    }

    @PostMapping(value = "/delete-by-id")
    public ResultResp delete(@Valid @RequestBody DeleteBookReq deleteBookReq){
        logger.info("<===== " + this.getClass().toString() + ": update book req: " + deleteBookReq.toString());
        return bookService.deleteBookById(deleteBookReq);
    }

    @PostMapping(value = "/search")
    public ResultResp search(@RequestBody SearchParamReq searchParamReq){
        logger.info("<===== " + this.getClass().toString() + ": search book req: " + searchParamReq.toString());
        return bookService.search(searchParamReq);
    }
}
