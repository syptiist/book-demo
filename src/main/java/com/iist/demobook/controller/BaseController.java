package com.iist.demobook.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by LeoJr
 * 2:57 PM 9/3/2019
 **/

public class BaseController {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
}
